//REQUIREMENT: One or more Classes (must use static methods and/or prototype methods)
class User {
  constructor(name, city){
    this.name = name;
    this.city = city;
  }
  setName(nameInput){
    this.name = nameInput;
  }
  setCity(cityInput){
    this.city = cityInput;
  }
}

//REQUIREMENT: One or more timing functions
//This function controls the fade-in of the document when it first refreshes
let body = document.querySelector("body");
let colorVal = 0;
const animate = function(){
    if(colorVal < 255){
        colorVal+=20
        body.style.backgroundColor = `rgb(${colorVal}, ${colorVal}, ${colorVal})`;
        requestAnimationFrame(animate)
    }
}
requestAnimationFrame(animate)

//Set the actions for what happens upon click
let inputForm = document.getElementById("user-info");
let inputForm2 = inputForm.cloneNode()
let submitButton = document.getElementById("submitButton");
let headerDoc = document.getElementById("headerTitle");
let bool = false;
let checkBool = false;

submitButton.addEventListener('click', ()=>{
  //If there is not a locally stored user, then create a new User and store locally
  //REQUIREMENT: Sets, updates, or changes local storage
  let user1 = new User("","")
  debugger
  if(localStorage.getItem("user1")==null && (inputForm[0].value < 1 || inputForm[1].value<1)){
    inputForm[0].classList.toggle("invalid")
    inputForm[1].classList.toggle("invalid")
    alert("Please enter a valid email or city")
    e.preventDefault()
  }
  else if(localStorage.getItem("user1")==null || bool == true){
    alert("updating user")
    user1.setName(inputForm[0].value);
    user1.setCity(inputForm[1].value);
    localStorage.setItem("user1",JSON.stringify(user1))
  }
  else{
    alert("A locally saved profile has already been found and will be loaded")
    let tempUser = JSON.parse(localStorage.getItem("user1"))
    user1.setName(tempUser.name);
    user1.setCity(tempUser.city);
  }
  //set the header info with username  
  headerDoc.innerHTML = `Good morning ${user1.name}!`;

  //REQUIREMENT: One or more fetch requests to a 3rd party API
  //Fetch the weather for the input city
  //Then set an output message with weather conditions and ok/not ok to bike
  const weatherURL = `https://api.openweathermap.org/data/2.5/weather?units=imperial&q=${user1.city}&appid=${API_KEY_WEATHER}`;
  fetch(weatherURL)
  .then(function(data) {
    console.log("youre here")
    return data.json();
  }).then(function(responseJson){
    console.log(responseJson);
    let bikingmsg = "It will be too cold to cycle today."
    if(responseJson.main.temp_min>45){
      bikingmsg = "It should be ok to cycle today."
    }
    //create bullets for each statement and append to html doc
    var node1 = document.createElement("LI");
    var node2 = document.createElement("LI");
    var node3 = document.createElement("LI");
    var weatherDesc = document.createTextNode(`${user1.city}'s weather today is ${responseJson.weather[0].description}.`) 
    var tempDesc = document.createTextNode(`You will see a high of ${responseJson.main.temp_max} 
    and a low of ${responseJson.main.temp_min} degrees F.`) 
    var bike = document.createTextNode(`${bikingmsg}`)
    node1.appendChild(weatherDesc);
    node2.appendChild(tempDesc)
    node3.appendChild(bike)
    document.body.appendChild(node1);
    document.body.appendChild(node2);
    document.body.appendChild(node3);
    inputForm.hidden = 1;

    //Add a button in case user wants to change name
    var btn = document.createElement("button")
    btn.setAttribute('class', 'btn btn-primary');
    btn.setAttribute('style', 'display: inline-block;');
    btn.innerHTML = 'Change User name or location';
    var br = document.createElement('br')
    headerDoc.appendChild(br);
    headerDoc.appendChild(btn);
    btn.addEventListener('click',()=>{
      bool = !bool;
      inputForm.hidden = 0;
      node1.remove()
      node2.remove()
      node3.remove()
    })
  })

  
  //return false;
  
})








